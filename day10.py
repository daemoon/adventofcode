import re

def look_and_say(input):
    a = re.finditer("(\d)\\1*", input)
    result = ""
    for i in a:
        text = i.group(0)
        result += str(len(text)) + text[0]
    return result



if __name__ == "__main__":
    input = "1113122113"
    for i in range(40):
        input = look_and_say(input)
    print(len(input))
    for i in range(10):
        input = look_and_say(input)
    print(len(input))