import re
password = 0
def first_rule(input):
    for letter_index in range(len(input)):
        try:
            if ord(input[letter_index]) == ord(input[letter_index+1]) -1 == ord(input[letter_index+2]) -2:
                return True
        except IndexError:
            return False

def second_rule(input):
    if input.__contains__('i') or input.__contains__('o') or  input.__contains__('l'):
        return False
    return True

def third_rule(input):
    matches = re.findall("(\w)\\1+", input)
    try:
        first_match = matches[0]
    except IndexError:
        return
    for match in matches[1:]:
        if match != first_match:
            return True
    return False


def increment_password(password, index=7):
    numeral_incremented = ord(password[index])+1
    if numeral_incremented > ord('z'):
        numeral_incremented = ord('a')
        password = increment_password(password,index-1)
    password_list= list(password)
    password_list[index] = chr(numeral_incremented)
    return "".join(password_list)

if __name__ == "__main__":
    password = "cqjxjnds"
    while not(first_rule(password) and second_rule(password) and third_rule(password)):
        password = increment_password(password)
    print(password)
    password = increment_password(password)
    while not(first_rule(password) and second_rule(password) and third_rule(password)):
        password = increment_password(password)
    print(password)
