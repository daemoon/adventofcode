import re
import json
if __name__=="__main__":
    file = open("day12in",'r')
    operators = {}
    input = file.read()
    numbers = [ int(x) for x in re.findall("(\d+|-\d+)", input)]
    print(numbers)
    print(sum(numbers))
 
