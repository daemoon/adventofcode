import re
import itertools
if __name__=="__main__":
    file = open("day13in",'r')
    lines = file.read().splitlines()
    relationships = {}
    for line in lines:
        match = re.match("(?P<name1>\w+) would (?P<change>lose|gain) (?P<amount>\d+) happiness units by sitting next to (?P<name2>\w+)\.", line)
        name1 = match.group('name1')
        name2 = match.group('name2')
        change = match.group('change')
        amount = int(match.group('amount'))
        if change == 'lose':
            amount = -amount
        try:
            relationships[name1][name2] = amount
        except KeyError:
            relationships[name1] = {name2: amount}
    print(relationships)
    happiness_of_combinations = []
    for combination in itertools.permutations(relationships.keys()):
        sum_of_happiness = 0
        for person_index in range(len(combination)):
            sum_of_happiness += relationships[combination[person_index]][combination[person_index-1]]
            try:
                sum_of_happiness += relationships[combination[person_index]][combination[person_index+1]]
            except IndexError:
                sum_of_happiness += relationships[combination[person_index]][combination[0]]
        happiness_of_combinations.append(sum_of_happiness)
    print(max(happiness_of_combinations))

    all_friends = relationships.keys()
    relationships["Me"] = {}
    for person in all_friends:
        relationships["Me"][person] = 0
        relationships[person]["Me"] = 0
    happiness_of_combinations = []
    for combination in itertools.permutations(relationships.keys()):
        sum_of_happiness = 0
        for person_index in range(len(combination)):
            sum_of_happiness += relationships[combination[person_index]][combination[person_index-1]]
            try:
                sum_of_happiness += relationships[combination[person_index]][combination[person_index+1]]
            except IndexError:
                sum_of_happiness += relationships[combination[person_index]][combination[0]]
        happiness_of_combinations.append(sum_of_happiness)
    print(max(happiness_of_combinations))

