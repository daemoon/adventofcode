import re

class reindeer:
    fly_speed = 0
    fly_duration = 0
    rest_duration = 0
    distance = 0
    points = 0
    def __init__(self, fly_speed, fly_duration, rest_duration):
        self.fly_speed = fly_speed
        self.fly_duration = fly_duration
        self.rest_duration = rest_duration

    def move_reindeer_move(self):
        self.distance += self.fly_speed

    def winner(self):
        self.points += 1


if __name__=="__main__":
    file = open("day14in",'r')
    lines = file.read().splitlines()
    race_time = 2503
    reindeers = {}
    for line in lines:
        match = re.match("(?P<name>\w+) can fly (?P<speed>\d+) km\/s for (?P<fly_time>\d+) seconds, but then must rest for (?P<rest_duration>\d+) seconds\.", line)
        reindeers[match.group("name")] = reindeer(int(match.group("speed")),int(match.group("fly_time")),int(match.group("rest_duration")))
    flight_amounts = []
    for reindeer, reindeer_stats in reindeers.items():
        race_period = (reindeer_stats.fly_duration + reindeer_stats.rest_duration)
        fly_intervals_whole = int(race_time / race_period)
        last_flight_time = (race_time % race_period) % reindeer_stats.fly_duration
        fly_length = ((fly_intervals_whole * reindeer_stats.fly_duration) + last_flight_time) * reindeer_stats.fly_speed
        flight_amounts.append(fly_length)
    print(max(flight_amounts))
    for race_moment in range(1,race_time+1):
        furthest_fly = 0
        furhest_reindeer = []
        for reindeer, reindeer_stats in reindeers.items():
            race_period = (reindeer_stats.fly_duration + reindeer_stats.rest_duration)
            flying = False
            if race_moment % race_period <= reindeer_stats.fly_duration:
                reindeers[reindeer].move_reindeer_move()
            if furthest_fly < reindeers[reindeer].distance:
                furthest_fly = reindeers[reindeer].distance
                furhest_reindeer = [reindeer]
            elif furthest_fly == reindeers[reindeer].distance:
                furhest_reindeer.append(reindeer)
        for x in furhest_reindeer:
            reindeers[x].winner()

    points = []
    for reindeer_stats in reindeers.values():
        points.append(reindeer_stats.points)
    print(max(points))



