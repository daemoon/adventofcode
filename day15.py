class ingredient:
    capacity = 0
    durability = 0
    flavor = 0
    texture = 0
    calories = 0

    def get_nutrition(self, count):
        return capacity * count + durability * count + texture * count + flavor * count
class sprinkles:
    capacity = 5
    durability = -1
    flavor = 0
    texture = 0
    calories = 5
class peanut_butter:
    capacity = -1
    durability = 3
    flavor = 0
    texture = 0
    calories = 1
class frosting:
    capacity = 0
    durability = -1
    flavor = 4
    texture = 0
    calories = 6
class sugar:
    capacity = -1
    durability = 0
    flavor = 0
    texture = 2
    calories = 8

import itertools
def return_nutritions(sprinkles_count, peanut_count, frosting_count, sugar_count):
    total_capacity = sprinkles_count * sprinkles.capacity + peanut_count * peanut_butter.capacity + frosting_count * frosting.capacity + sugar_count * sugar.capacity
    total_durability = sprinkles_count * sprinkles.durability + peanut_count * peanut_butter.durability + frosting_count * frosting.durability + sugar_count * sugar.durability
    total_flavor = sprinkles_count * sprinkles.flavor + peanut_count * peanut_butter.flavor + frosting_count * frosting.flavor + sugar_count * sugar.flavor
    total_texture = sprinkles_count * sprinkles.texture + peanut_count * peanut_butter.texture + frosting_count * frosting.texture + sugar_count * sugar.texture
    total_calories = sprinkles_count * sprinkles.calories + peanut_count * peanut_butter.calories + frosting_count * frosting.calories + sugar_count * sugar.calories
    if total_calories != 500:
        return -1
    total_capacity, total_durability, total_flavor, total_texture = [x if x > 0 else 0 for x in ([total_capacity, total_durability, total_flavor, total_texture])]

    summary = total_capacity*total_durability*total_flavor*total_texture
    return  0 if summary < 0 else summary
if __name__ == "__main__":
    ingredients = [sprinkles, peanut_butter, frosting, sugar]
    max_nutritions = 0
    for i in range(101):
        for j in range(101):
            for k in range(101):
                for l in range(101):
                    if i + j + k + l != 100:
                        continue
                    total = return_nutritions(i,j,k,l)
                    if total > max_nutritions:
                        max_nutritions = total

    print(max_nutritions)

