import re
if __name__ == "__main__":
    file = open("day16in",'r')
    lines = file.read().splitlines()
    aunts= {}
    for line in lines:
        match = re.match("Sue (\d+): (.*)", line)
        parameters = match.group(2).replace(" ", "").split(",")
        aunt_number = int(match.group(1))
        aunt_parameters = {}
        for parameter in parameters:
            key_value = re.match("(\w+):(\d+)", parameter)
            aunt_parameters[key_value.group(1)] = int(key_value.group(2))
        aunts[aunt_number] =  aunt_parameters
    looked_aunt = {"children" : 3, "cats":7, "samoyeds":2, "pomeranians" : 3, "akitas":0, "vizslas":0, "goldfish":5, "trees": 3, "cars":2,"perfumes":1}
    results = set()
    for aunt_num, aunt_params in aunts.items():
        match = 0
        for key, param in aunt_params.items():
            if (key == "cats" or key == "trees") and looked_aunt[key] < param:
               match += 10
            elif (key == "pomeranians" or key == "goldfish") and looked_aunt[key] > param:
                match += 10
            elif looked_aunt[key] == param:
                match += 10
            else:
                match = 0
                break
        if match > 0:
            results.add((aunt_num, match))
    print(results)
