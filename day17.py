def combinations(amount, jugs):
    ways = [0] * (amount + 1)
    ways[0] = 1
    for jug in jugs:
        for j in range(jug, amount + 1):
            ways[j] += ways[j - jug]
    return ways[amount]

if __name__=="__main__":
    file = open("day17in",'r')
    amount = 150
    jugs = file.read().splitlines()
    jugs = [ int(x) for x in jugs ]
    valid_combinations_count = 0
    minimum = 0
    minimal_combinations_count = 0
    import itertools
    for i in range(1, len(jugs)):
        combination = itertools.combinations(jugs, i)
        for option in combination:
            if sum(option) == amount:
                valid_combinations_count += 1
                if minimum == 0:
                    minimum = i
                if i == minimum:
                    minimal_combinations_count += 1

    print(valid_combinations_count)
    print(minimal_combinations_count)

