def calculate_lit_neighbors(neigbors):
    return 0

class NoWrap(object):
    def __init__(self, obj, default=None):
        self._obj = obj
        self._default = default

    def __getitem__(self, key):
        if isinstance(key,int):
            if key < 0:
                return None
        return self._obj.__getitem__(key)

if __name__ == "__main__":
    f = open("day18in", 'r')
    input = f.read().splitlines()
    lights = [[]]
    for line in input:
        lights.append(NoWrap(list(line)))
    lights = NoWrap(lights)
    print(lights[2][-1])
    #for light_row, row in enumerate(lights):
    #    for light, column in enumerate(light_row):
    #        neighbors = lights[row-1][column-1:column+1]
