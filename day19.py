if __name__=="__main__":
    input = open("day19in", 'r')
    lines = input.read().splitlines()
    equations = []
    import re
    for line in lines:
        if line != '':
            match = re.search("(\w+) => (\w+)", line)
            equations.append((match.group(1), match.group(2)))
        else:
            break
    chemical = lines[-1]
    mutations = set()
    for i in range(0, len(chemical)):
        for pattern in equations:
            mutation = chemical[:i]+chemical[i:].replace(pattern[0],pattern[1],1)
            mutations.add(mutation)

    print(len(mutations)-1)

