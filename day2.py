if __name__ == "__main__":
    file = open("day2in",'r')
    lines = file.read().splitlines()
    sumOfRibbon = 0
    sumOfPaper = 0
    for line in lines:
        parameters = line.split("x")
        length = int(parameters[0])
        width = int(parameters[1])
        height = int(parameters[2])
        bottomSide = length*width*2
        leftSide = length*height*2
        frontSide = height*width*2
        sumOfPaper += bottomSide+leftSide+frontSide+min(bottomSide,leftSide,frontSide)/2
        sorted_sides = sorted([length, width, height])
        sumOfRibbon += (sorted_sides[0]+sorted_sides[1]) * 2 + (length*width*height)
    print("Paper: " + str(sumOfPaper))
    print("Ribbon: " + str(sumOfRibbon))
