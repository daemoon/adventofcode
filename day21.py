class item:
    cost = 0
class weapon(item):
    damage = 0
class dagger(weapon):
    damage = 4
    cost = 8
class shortsword(weapon):
    damage = 5
    cost = 10
class warhammer(weapon):
    damage = 6
    cost = 25
class longsword(weapon):
    damage = 7
    cost = 40
class greataxe(weapon):
    damage = 8
    cost = 74

class armor(item):
    armor = 0
    cost = 0
class barenaked(armor):
    cost = 0
    armor = 0
class leather(armor):
    cost = 13
    armor = 1
class chainmail(armor):
    cost = 31
    armor = 2
class splintmail(armor):
    cost = 53
    armor = 3
class bandemail(armor):
    cost = 75
    armor = 4
class platemail(armor):
    cost = 102
    armor = 5
class ring(item):
    cost = 0
    damage = 0
    armor = 0
class dmg1(ring):
    cost = 25
    damage = 1
    armor = 0
class dmg2(ring):
    cost = 50
    damage = 2
    armor = 0
class dmg3(ring):
    cost = 100
    damage = 3
    armor = 0
class armor1(ring):
    cost = 20
    damage = 0
    armor = 1
class armor2(ring):
    cost = 40
    damage = 0
    armor = 2
class armor3(ring):
    cost = 80
    damage = 0
    armor = 3
class noring(ring):
    cost = 0
    damage = 0
    armor = 0

from operator import attrgetter
import math
def would_survive(your_hp, your_dmg, your_armor, enemy_hp, enemy_dmg, enemy_armor):
    def calculate_turns(hp, dmg, armor):
        try:
            return math.ceil(hp / (dmg - armor))
        except ZeroDivisionError:
            return 10000
    turns_to_survive = calculate_turns(your_hp, enemy_dmg, your_armor)
    turns_to_kill = calculate_turns(enemy_hp, your_dmg, enemy_armor)
    if turns_to_survive + 1 < turns_to_kill:
        return False
    return True

def find_cheapest_combo(dmg, armor):
    sum_dmg = 0
    sum_armor = 0
    sum_cost = 0
    min_cost = 20000
    for weapon in weapons:
        for armor_gear in armors:
            for index,ring1 in enumerate(rings):
                for ring2 in rings[index+1:]:
                    sum_cost = ring2.cost + ring1.cost + weapon.cost + armor_gear.cost
                    sum_armor = ring2.armor + ring1.armor + armor_gear.armor
                    sum_dmg = ring2.damage + ring1.damage + weapon.damage
                    if sum_armor >= armor and sum_dmg >= dmg and sum_cost < min_cost:
                        min_cost = sum_cost
    return min_cost
def find_most_expensive_combo(dmg, armor):
    sum_dmg = 0
    sum_armor = 0
    sum_cost = 0
    max_cost = 0
    for weapon in weapons:
        for armor_gear in armors:
            for index,ring1 in enumerate(rings):
                for ring2 in rings[index+1:]:
                    sum_cost = ring2.cost + ring1.cost + weapon.cost + armor_gear.cost
                    sum_armor = ring2.armor + ring1.armor + armor_gear.armor
                    sum_dmg = ring2.damage + ring1.damage + weapon.damage
                    if sum_armor == armor and sum_dmg == dmg and sum_cost > max_cost:
                        max_cost = sum_cost
    return max_cost





if __name__ == "__main__":
    class boss:
        defense = 2
        damage = 8
        hit_points = 100
    your_hp = 100
    min_dmg = boss.defense + 1
    weapons = [dagger, shortsword, warhammer, longsword, greataxe]
    armors = [barenaked, leather, chainmail, splintmail, bandemail, platemail]
    rings = [dmg1, dmg2, dmg3, armor1, armor2, armor3, noring, noring]
    max_dmg = max(weapons, key=attrgetter('damage')).damage + max(rings, key=attrgetter('damage')).damage
    min_armor = 0
    max_armor = max(armors, key=attrgetter('armor')).armor + max(rings, key=attrgetter('armor')).armor
    survival_combinations = []
    for dmg in range(min_dmg, max_dmg+1):
        for armor in range(min_armor, max_armor+1):
            if would_survive(your_hp, dmg, armor, boss.hit_points, boss.damage,boss.defense):
                survival_combinations.append({'dmg': dmg, 'armor':armor})
                break
    print(survival_combinations)
    min_cost = 20000
    cost = 0
    for combo in survival_combinations:
        cost = find_cheapest_combo(combo['dmg'], combo['armor'])
        if cost < min_cost:
            min_cost = cost
    print(min_cost)
    killing_combination = []
    for dmg in range(0, max_dmg+1):
        for armor in range(0, max_armor+1):
            if not would_survive(your_hp, dmg, armor, boss.hit_points, boss.damage,boss.defense):
                killing_combination.append({'dmg': dmg, 'armor':armor})

    print(killing_combination)
    max_cost = 0
    cost = 0
    for combo in killing_combination:
        cost = find_most_expensive_combo(combo['dmg'], combo['armor'])
        if cost > max_cost:
            max_cost = cost
    print(max_cost)