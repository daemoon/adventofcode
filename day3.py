if __name__ == "__main__":
    file = open("day3in",'r')
    input = file.read()
    visitedHouses = {(0,0)}
    x = 0
    y = 0
    for direction in input:
        if direction == "<":
            x -= 1
        elif direction == ">":
            x += 1
        elif direction == "v":
            y -= 1
        elif direction == "^":
            y += 1
        visitedHouses.add((x,y))
    print(len(visitedHouses))
    visitedHouses = {(0,0)}
    roboSanta = False
    coords = {'santa' : {'x':0, 'y':0}, 'robo' : {'x':0, 'y':0}}
    for direction in input:
        if roboSanta:
            turn = 'robo'
        else:
            turn = 'santa'

        x = coords[turn]['x']
        y = coords[turn]['y']
        if direction == "<":
            x -= 1
        elif direction == ">":
            x += 1
        elif direction == "v":
            y -= 1
        elif direction == "^":
            y += 1
        coords[turn]['x'] = x
        coords[turn]['y'] = y
        roboSanta = ~roboSanta
        visitedHouses.add((x,y))
    print(len(visitedHouses))







