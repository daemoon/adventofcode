import re
if __name__ == "__main__":
    file = open("day5in",'r')
    lines = file.read().splitlines()
    nice_lines = 0
    firstRuleOfNiceness = re.compile("((a|e|i|o|u).*(a|e|i|o|u).*(a|e|i|o|u))")
    secondRuleOfNiceness = re.compile('(\w)\\1')
    thirdRuleOfNaughtiness = re.compile("ab|cd|pq|xy")
    for line in lines:
        if firstRuleOfNiceness.search(line) is not None:
            if secondRuleOfNiceness.search(line) is not None:
                if thirdRuleOfNaughtiness.search(line) is None:
                    nice_lines+=1
    print(nice_lines)
    file = open("day5in",'r')
    lines = file.read().splitlines()
    nice_lines_new = 0
    newFirstRuleOfNiceness = re.compile(".*(\w\w).*\\1")
    newSecondRuleOfNiceness = re.compile(".*(\w)\w\\1")
    for line in lines:
        if newFirstRuleOfNiceness.match(line) is not None:
            if newSecondRuleOfNiceness.match(line) is not None:
                nice_lines_new += 1
    print(nice_lines_new)

