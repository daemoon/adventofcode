from enum import Enum
import re
class instruction_type_enum(Enum):
    turn_on = 1
    turn_off = 2
    toggle = 3
class instruction:
    instruction_type = 0
    x1 = 0
    x2 = 0
    y1 = 0
    y2 = 0

    def __init__(self, input_string):
        match = re.match("(turn on|toggle|turn off) (\d+),(\d+) through (\d+),(\d+)", input_string)
        instruction = match.group(1)
        if instruction == "turn on":
            self.instruction_type = instruction_type_enum.turn_on
        elif instruction == "turn off":
            self.instruction_type = instruction_type_enum.turn_off
        elif instruction == "toggle":
            self.instruction_type = instruction_type_enum.toggle
        self.x1 = int(match.group(2))
        self.y1 = int(match.group(3))
        self.x2 = int(match.group(4))
        self.y2 = int(match.group(5))

if __name__ == "__main__":
    file = open("day6in",'r')
    lines = file.read().splitlines()
    shiny_lights = set()
    lightning_light = {}
    for line in lines:
        instruction_instance = instruction(line)
        for j in range(instruction_instance.y1,instruction_instance.y2+1):
            for i in range(instruction_instance.x1,instruction_instance.x2+1):
                light = (i,j)
                if instruction_instance.instruction_type == instruction_type_enum.turn_on:
                    shiny_lights.add(light)
                    try:
                        lightning_light[light] += 1
                    except KeyError:
                        lightning_light[light] = 1
                elif instruction_instance.instruction_type == instruction_type_enum.turn_off:
                    try:
                        shiny_lights.remove(light)

                    except:
                        pass
                    try:
                        if lightning_light[light] > 0:
                            lightning_light[light] -= 1
                    except:
                        pass
                elif instruction_instance.instruction_type == instruction_type_enum.toggle:
                    try:
                        shiny_lights.remove(light)
                    except KeyError:
                        shiny_lights.add(light)
                    try:
                        lightning_light[light] += 2
                    except KeyError:
                        lightning_light[light] = 2
    print(len(shiny_lights))
    print(sum(lightning_light.values()))

