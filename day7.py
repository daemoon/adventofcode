import re

class component(object):
    def __init__(self, name, subcomponent1, operator, subcomponent2):
        self.name = name
        self.subcomponent1 = subcomponent1
        self.operator = operator
        self.subcomponent2 = subcomponent2
    def evaluate(self):
        retval = self.operator(self.subcomponent1, self.subcomponent2)
        components[self.name] = component(self.name, retval, VALUE, 0)
        return retval

def AND(arg1, arg2):
    return components[arg1].evaluate() & components[arg2].evaluate()
def OR(arg1, arg2):
    return(components[arg1].evaluate() | components[arg2].evaluate())
def LSHIFT(arg1, arg2):
    return components[arg1].evaluate() << int(arg2)
def RSHIFT(arg1, arg2):
    return components[arg1].evaluate() >> int(arg2)
def VALUE(arg1, arg2):
    return int(arg1)
def NOT(arg1, arg2):
    return ~components[arg1].evaluate()
def MOVE(arg1, arg2):
    return components[arg1].evaluate()
def generate_new_variable(variable_value):
    variable_name = "tmp" + str(variable_value)
    if variable_name not in components.keys():
        components[variable_name] = components[variable_name] = component(variable_name, variable_value, VALUE, 0)
    return variable_name

if __name__ == "__main__":
    file = open("day7in",'r')
    operators = {}
    lines = file.read().splitlines()
    components = {}
    for line in lines:
        valueGate = re.match("(\d+) -> (\w+)",line)
        moveGate = re.match("(\w+) -> (\w+)",line)
        binaryOpGate = re.match("(\w+) (AND|OR|LSHIFT|RSHIFT) (\w+) -> (\w+)",line)
        unaryOpGate = re.match("NOT (\w+) -> (\w+)",line)
        if valueGate is not None:
            components[valueGate.group(2)] = component(valueGate.group(2), valueGate.group(1), VALUE, 0)
        elif binaryOpGate is not None:
            op1 = binaryOpGate.group(1)
            operation = binaryOpGate.group(2)
            op2 = binaryOpGate.group(3)
            if operation == "AND":
                operation = AND
            elif operation == "OR" :
                operation = OR
            elif operation == "RSHIFT":
                operation = RSHIFT
            elif operation == "LSHIFT":
                operation = LSHIFT
            if operation == OR or operation == AND:
                try:
                    numeral = int(op1)
                    op1 = generate_new_variable(numeral)
                except ValueError:
                    pass
                try:
                    numeral = int(op2)
                    op2 = generate_new_variable(numeral)
                except ValueError:
                    pass
            components[binaryOpGate.group(4)] = component(binaryOpGate.group(4), op1, operation, op2)
        elif unaryOpGate is not None:
            components[unaryOpGate.group(2)] = component(unaryOpGate.group(2), unaryOpGate.group(1), NOT, 0)
        elif moveGate is not None:
            components[moveGate.group(2)] = component(moveGate.group(2), moveGate.group(1), MOVE, 0)
    components['b'] = component('b', 3176, VALUE, 0)
    print(components['a'].evaluate())




