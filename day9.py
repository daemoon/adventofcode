import re
import itertools

def calculate_distance(input):
    try:
        distance = destinations[input[0]][input[1]]
    except IndexError:
        return 0
    return distance+calculate_distance(input[1:])

if __name__ == "__main__":
    file = open("day9in",'r')
    lines = file.read().splitlines()
    destinations = {}
    for line in lines:
        match = re.match("(\w+) to (\w+) = (\d+)",line)
        from_place = match.group(1)
        to_place = match.group(2)
        distance = int(match.group(3))
        try:
            destinations[from_place][to_place]=distance
        except KeyError:
            destinations[from_place] = {to_place:distance}
        try:
            destinations[to_place][from_place]=distance
        except KeyError:
            destinations[to_place] = {from_place:distance}
    distances = []
    for combination in itertools.permutations(destinations.keys(), len(destinations.keys())):
        distances.append(calculate_distance(combination))
    print(min(distances))
    print(max(distances))





